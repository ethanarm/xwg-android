#include <jni.h>
#include <string>

extern "C" JNIEXPORT jstring

JNICALL
Java_org_ethanarm_xwg_1android_MainActivity_stringFromJNI(
        JNIEnv *env,
        jobject /* this */) {


    std::string hello = "Hello from C++";
    int i = 42;
    hello += std::to_string(i);
    return env->NewStringUTF(hello.c_str());
}

extern "C"
JNIEXPORT jstring JNICALL
Java_org_ethanarm_xwg_1android_MainActivity_callMeGay(JNIEnv *env, jobject instance) {

    std::string ugay = "U R Gay";


    return env->NewStringUTF(ugay.c_str());
}